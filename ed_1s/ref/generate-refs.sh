#!/bin/bash
set -eoux pipefail
mv ../interm/*_dashinit.mp4 ../interm/*.mpd .
for vid in *.mpd
do
	../../generate-scripts/split-manifest.sh $vid
	../../generate-scripts/glue-init.sh ${vid%.mpd}
done
mv *.mp4 *.mpd ../interm
