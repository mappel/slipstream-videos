#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-sintel.mpd sintel_2160p_10k_24fps_96key_300s_dash.mpd sintel_1440p_7k4_24fps_96key_300s_dash.mpd sintel_1080p_5k8_24fps_96key_300s_dash.mpd sintel_1080p_4k3_24fps_96key_300s_dash.mpd sintel_720p_3k_24fps_96key_300s_dash.mpd sintel_720p_2k35_24fps_96key_300s_dash.mpd sintel_480p_1k75_24fps_96key_300s_dash.mpd sintel_480p_1k05_24fps_96key_300s_dash.mpd sintel_360p_0k75_24fps_96key_300s_dash.mpd sintel_360p_0k56_24fps_96key_300s_dash.mpd sintel_240p_0k375_24fps_96key_300s_dash.mpd sintel_240p_0k235_24fps_96key_300s_dash.mpd sintel_144p_0k16_24fps_96key_300s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-sintel.mpd
../mpd-scripts/make-ful-rel.py slipstream-sintel.mpd reliable-sintel.mpd
mv *_dash.mpd interm
