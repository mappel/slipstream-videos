#!/bin/bash
if [ ! $# -eq 1 ]
then
    echo "usage: $0 <threshold>"
    exit
fi
THRESHOLD="$1"
rm log.txt chains.txt params.txt 2> /dev/null
for GRAPH_DIR in *-graphs
do
    PREFIX=${GRAPH_DIR%-graphs}
    DIR="$PREFIX"-chain-weights-"$THRESHOLD"p
    RESOLUTION=$(echo "$DIR" | grep -o '[0-9]\{3,\}p')
    RESOLUTION=${RESOLUTION%p}
    mkdir -p "$DIR"
    for SEGMENT in "$GRAPH_DIR"/*.dat
    do
        SEGMENT_NAME=$(basename $SEGMENT)
        echo "$SEGMENT" >> log.txt
        echo "$DIR"/"$SEGMENT_NAME" >> chains.txt
        echo "$RESOLUTION" >> params.txt
    done
done
parallel /pool/data/mappel/mb-parser/build/chain_weights {1} {2} "$THRESHOLD" --absolute {3} :::: log.txt ::::+ chains.txt ::::+ params.txt
rm log.txt chains.txt params.txt
