#!/bin/bash
for VIDEO in *.h264
do
    PREFIX=${VIDEO%.h264}
    WEIGHT_DIR="$PREFIX"-weights
    OUTPUT_DIR="$PREFIX"-ordered-weights
    ./get-ordered-weights.py "$WEIGHT_DIR" "$OUTPUT_DIR" "$PREFIX"
done
