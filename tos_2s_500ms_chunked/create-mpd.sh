#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-tos.mpd tos_2s_500ms_chunked_1080p_6k_24fps_48key_300s_dash.mpd tos_2s_500ms_chunked_720p_3k_24fps_48key_300s_dash.mpd tos_2s_500ms_chunked_480p_1k2_24fps_48key_300s_dash.mpd tos_2s_500ms_chunked_360p_0k6_24fps_48key_300s_dash.mpd tos_2s_500ms_chunked_160p_0k2_24fps_48key_300s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-tos.mpd
../mpd-scripts/make-ful-rel.py slipstream-tos.mpd reliable-tos.mpd
mv *_dash.mpd interm
