#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-ed.mpd ed_4s_500ms_chunked_1080p_6k_24fps_96key_300s_dash.mpd ed_4s_500ms_chunked_720p_3k_24fps_96key_300s_dash.mpd ed_4s_500ms_chunked_480p_1k2_24fps_96key_300s_dash.mpd ed_4s_500ms_chunked_360p_0k6_24fps_96key_300s_dash.mpd ed_4s_500ms_chunked_160p_0k2_24fps_96key_300s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-ed.mpd
../mpd-scripts/make-ful-rel.py slipstream-ed.mpd reliable-ed.mpd
mv *_dash.mpd interm
