#!/usr/bin/python3
from collections import namedtuple
import math
import os
import sys

FileInfo = namedtuple('FileInfo', 'path basename segment_no')
Reference = namedtuple('Reference', 'frame mb_count referenced_by')

DAT_SUFFIX = '.dat'


def get_fileinfo(path: str, name: str):
    components = name[:-len(DAT_SUFFIX)].split('-')
    if len(components) > 2:
        print('Warning: Filename split into more than two parts:', name, file=sys.stderr)
    basename = '-'.join(components[:-1])
    segment_no = components[-1]
    return FileInfo(path + name, basename, segment_no)


def get_input_files(input_path: str):
    ret = list()
    it = os.scandir(input_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(DAT_SUFFIX):
            ret.append(get_fileinfo(input_path, entry.name))
    return ret


def check_reference(reference: Reference, chunk_frames: int):
    """Check if the line is a intra-chunk reference."""
    # Keep sink lines, which always use frame 0 as a reference with a weight of
    # zero.
    if reference.mb_count == 0:
        return True
    # These are actually slice numbers so they increase in steps of 2.
    chunk_frames_times_2 = chunk_frames * 2
    frame_num = reference.frame
    ref_frame_num = reference.referenced_by
    # Chunk number is zero-indexed.
    chunk_no = math.floor(frame_num / chunk_frames_times_2)
    min_allowed_ref = chunk_no * chunk_frames_times_2
    max_allowed_ref = min_allowed_ref + chunk_frames_times_2 - 2
    if ref_frame_num < min_allowed_ref or ref_frame_num > max_allowed_ref:
        return False
    return True


def process_file(info: FileInfo, chunk_frames: int):
    """Filter out lines that cross chunk boundaries."""
    ret = list()
    lines = list()
    with open(info.path, 'r') as f:
        # Consume header
        ret.append(f.readline())
        for line in f:
            line_split = line.strip().split()
            if len(line_split) != 3:
                print('Error: Invalid line:', line.strip(), file=sys.stderr)
                continue
            lines.append(Reference(int(line_split[0]), int(line_split[1]), int(line_split[2])))
    valid_refs = list()
    for idx, ref in enumerate(lines):
        if check_reference(ref, chunk_frames):
            valid_refs.append(ref)
        else:
            # Check if this is the only or last occurence of this frame that
            # would be dropped. Insert a sink if this is the case.
            # Frame is already present in valid reference list.
            if len(valid_refs) > 0 and valid_refs[-1].frame == ref.frame:
                continue
            # Another reference for this frame follows, which might be valid.
            if idx + 1 < len(lines) and lines[idx + 1].frame == ref.frame:
                continue
            # Insert sink
            valid_refs.append(Reference(ref.frame, 0, 0))
    for ref in valid_refs:
        ret.append(' '.join(map(str, ref)) + '\n')
    return ret


def write_output(output_path: str, output_lines: list, info: FileInfo):
    with open(output_path + info.basename + '-' + info.segment_no + DAT_SUFFIX, 'w') as f:
        f.writelines(output_lines)


if __name__ == '__main__':
    if len(sys.argv) != 4:
        print('usage:',sys.argv[0],'<path/to/input> <path/to/output> <frames in chunk>')
        exit(1)
    input_path = sys.argv[1]
    if not input_path.endswith('/'):
        input_path += '/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    chunk_frames = int(sys.argv[3])
    input_files = get_input_files(input_path)
    for info in input_files:
        output_lines = process_file(info, chunk_frames)
        write_output(output_path, output_lines, info)

