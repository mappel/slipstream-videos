#!/bin/bash
if [ ! $# -eq 1 ]
then
    echo "usage: $0 <frames in chunk>"
    exit
fi
FRAMES="$1"

for INPUT_FOLDER in *-graphs
do
    OUTPUT_FOLDER="${INPUT_FOLDER}-${FRAMES}f-chunks"
    mkdir -p "$OUTPUT_FOLDER"
    ./cut-edges.py "$INPUT_FOLDER" "$OUTPUT_FOLDER" "$FRAMES"
done

