#!/usr/bin/python3
# This script can be used to quickly generate the average bitrate of a specific range of segments from a
# segment-sizes.dat file.
import sys

SEGMENT_LENGTH_IN_S=4

if len(sys.argv) != 4:
    print('usage: ' + sys.argv[0] + ' <segment-sizes> <segment-start> <segment-count>')
    exit(1)
segment_sizes_file = sys.argv[1]
start = int(sys.argv[2])
count = int(sys.argv[3])
if start <= 0:
    print('Error: segment-start value must be greater than 0.', file=sys.stderr)
    exit(1)
segment_sizes = list()
with open(segment_sizes_file, 'r') as f:
    for line in f:
        segment_sizes.append(int(line))
if len(segment_sizes) == 0:
    print('Error: No segment sizes found in file: ' + segment_sizes_file, file=sys.stderr)
    exit(1)
# Insert 0 byte segment if it does not exist. This is used for pretty plotting and to keep the list index synchronized
# with the segment numbers.
if segment_sizes[0] != 0:
    segment_sizes.insert(0, 0)
if start > len(segment_sizes) or start + count > len(segment_sizes):
    print('Error: segment-start or end of range is out of bounds.', file=sys.stderr)
    # Print the actual number of segments, excluding the initial zero placeholder segment.
    print('Error: segment-start: ' + str(start) + ' end: ' + str(start + count - 1) + ' len: ' + str(len(segment_sizes) - 1), file=sys.stderr)
    exit(1)
size_acc = 0
# count includes the start segment, so we only want to iterate until start + count - 1.
for i in range(start, start + count):
    size_acc += segment_sizes[i]
size_kbit = size_acc * 8 / 1000
avg_bitrate = size_kbit / count / SEGMENT_LENGTH_IN_S
print(str(avg_bitrate) + ' kbps')
