#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-ed.mpd ed_1080p_10k_24fps_96key_300s_dash.mpd ed_1080p_7k4_24fps_96key_300s_dash.mpd ed_1080p_5k8_24fps_96key_300s_dash.mpd ed_1080p_4k3_24fps_96key_300s_dash.mpd ed_720p_3k_24fps_96key_300s_dash.mpd ed_720p_2k35_24fps_96key_300s_dash.mpd ed_480p_1k75_24fps_96key_300s_dash.mpd ed_480p_1k05_24fps_96key_300s_dash.mpd ed_360p_0k75_24fps_96key_300s_dash.mpd ed_360p_0k56_24fps_96key_300s_dash.mpd ed_240p_0k375_24fps_96key_300s_dash.mpd ed_240p_0k235_24fps_96key_300s_dash.mpd ed_144p_0k16_24fps_96key_300s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-ed.mpd
../mpd-scripts/make-ful-rel.py slipstream-ed.mpd reliable-ed.mpd
mv *_dash.mpd interm
