#!/bin/bash
mkdir -p frame-types
for folder in *-weights-sizes
do
	VIDEO=${folder%-weights-sizes}
	rm $folder/$VIDEO-I-frames.dat 2> /dev/null
	rm $folder/$VIDEO-P-frames.dat 2> /dev/null
	rm $folder/$VIDEO-B-frames.dat 2> /dev/null
	for i in $(seq 1 75)
	do
		awk '$2 == "I" {s+=$4;n+=1} END {print s" "n}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-I-frames.dat
		awk '$2 == "P" {s+=$4;n+=1} END {print s" "n}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-P-frames.dat
		awk '$2 == "B" {s+=$4;n+=1} END {print s" "n}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-B-frames.dat
	done
	rm frame-types/$VIDEO-frames 2> /dev/null
	awk '{s+=$1;n+=$2} END {print "I: "s" "n}' $folder/$VIDEO-I-frames.dat >> frame-types/$VIDEO-frames.dat
	awk '{s+=$1;n+=$2} END {print "P: "s" "n}' $folder/$VIDEO-P-frames.dat >> frame-types/$VIDEO-frames.dat
	awk '{s+=$1;n+=$2} END {print "B: "s" "n}' $folder/$VIDEO-B-frames.dat >> frame-types/$VIDEO-frames.dat
done
