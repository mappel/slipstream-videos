#!/bin/bash
rm vids.txt sizes.txt 2> /dev/null
for vid in *.h264
do
	PREFIX=${vid%.h264}
	DIR="$PREFIX"-sizes
	mkdir -p "$DIR"
	echo "$PREFIX"_dashinit.mp4 >> vids.txt
	echo "$DIR"/"$PREFIX" >> sizes.txt
done
parallel --bar /pool/data/mappel/header_parser/build/header_parser {1} --info {2} :::: vids.txt ::::+ sizes.txt
rm vids.txt sizes.txt
