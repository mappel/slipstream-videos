set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 back ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics back ls 81, ls 81, ls 82

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify

set style line 1 lc rgb "#2a71b0" lw 0.5
set style line 2 lc rgb "#e32322" lw 0.5
set style line 3 lc rgb "#f4e500" lw 0.5
set style line 4 lc rgb "#444e99" lw 0.5
set style line 5 lc rgb "#ea621f" lw 0.5
set style line 6 lc rgb "#8cbb26" lw 0.5
set style line 7 lc rgb "#6d398b" lw 0.5
set style line 8 lc rgb "#f18e1c" lw 0.5
set style line 9 lc rgb "#008e5b" lw 0.5
set style line 10 lc rgb "#c4037d" lw 0.5
set style line 11 lc rgb "#fdc60b" lw 0.5
set style line 12 lc rgb "#0696bb" lw 0.5
set style line 13 lc rgb "#e3228f" lw 0.5

unset key

set xrange [0:75]
set xtics 0,10 offset 0,0.5
set mxtics 2
set xlabel "Segments" offset 0,1

set yrange [0:]
set ylabel "Throughput (in Mbps)" font ',18' offset 1,0

set output OUT_FILE
plot \
     IN_FILE u ($0):($1*8/1000000/4):(0) w filledcurves ls 1
unset output
