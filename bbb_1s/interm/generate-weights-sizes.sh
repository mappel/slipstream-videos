#!/bin/bash
rm vids.txt out.txt weights.txt 2> /dev/null
for vid in *.h264
do
	PREFIX=${vid%.h264}
	WEIGHTS_DIR="$PREFIX"-weights
	OUT_DIR="$PREFIX"-weights-sizes
	mkdir -p "$OUT_DIR"
	echo "$PREFIX"_dashinit.mp4 >> vids.txt
	echo "$WEIGHTS_DIR"/"$PREFIX" >> weights.txt
	echo "$OUT_DIR"/"$PREFIX" >> out.txt
done
parallel --bar /pool/data/mappel/header_parser/build/header_parser {1} --weights {2} --info {3} :::: vids.txt ::::+ weights.txt ::::+ out.txt
rm vids.txt out.txt weights.txt
