#!/bin/bash
set -eoux pipefail
for WEIGHT_DIR in *-weights
do
	PREFIX=${WEIGHT_DIR%-weights}
	SIZE_DIR="$PREFIX"-sizes
	if [ ! -d $SIZE_DIR ]
	then
		echo "Error: No matching sizes directory for weights directory: $WEIGHT_DIR"
		continue
	fi
	OUT_DIR="$PREFIX"-weights-sizes-bytestream
	mkdir -p "$OUT_DIR"
	for WEIGHT_FILE in "$WEIGHT_DIR"/*.dat
	do
		SIZE_FILE=$(basename "$WEIGHT_FILE")
		./merge-weights-sizes.py "$SIZE_DIR"/"$SIZE_FILE" "$WEIGHT_FILE" "$OUT_DIR"/"$SIZE_FILE"
	done
done
