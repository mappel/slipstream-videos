slipstream-original-bbb.mpd: Frames are in original order. Representations
  contain avgSSIM attributes that specify the average SSIM for the entire video.
slipstream-original-bbb-vmaf.mpd: Same as above, but avgSSIM tag actually
  specifies average VMAF score (mapped to [0,1])
slipstream-original-bbb-psnr.mpd: Same as above, but avgSSIM tag actually
  specifies average PSNR score (mapped to [0,60])
slipstream-ordered-bbb.mpd: Frames are ordered in original order, expect for
  unreferenced B-frames, which are moved to the back. No avgSSIM attribute.
slipstream-chain-1p-bbb.mpd: Frames are ordered in chain order. No avgSSIM
  attribute.
