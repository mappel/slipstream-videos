#!/usr/bin/python3
import sys

if len(sys.argv) != 4:
    print('usage: ' + sys.argv[0] + ' <size-file> <weight-file> <output>')
    exit(1)
size_file_path = sys.argv[1]
weight_file_path = sys.argv[2]
out_file_path = sys.argv[3]
weight_file = open(weight_file_path, 'r')
out_lines = list()
with open(size_file_path, 'r') as size_file:
    for size_line in size_file:
        weight_line = weight_file.readline()
        if weight_line == '':
            print('Error: Weight file ended before size file.')
            exit(1)
        size_line_split = size_line.split()
        if len(size_line_split) != 4:
            print('Error: Invalid size line: ' + size_line)
            exit(1)
        weight_line_split = weight_line.split()
        if len(weight_line_split) != 2:
            print('Error: Invalid weight line: ' + weight_line)
            exit(1)
        out_lines.append(size_line_split[0] + ' ' + size_line_split[1] + ' ' + weight_line_split [1] + ' ' + size_line_split[3] + '\n')
with open(out_file_path, 'w') as f:
    f.writelines(out_lines)
