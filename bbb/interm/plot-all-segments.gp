set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid size 14,35
set output OUT_FILE
set multiplot layout 15,5 margins screen .05, .95, .08, .93 spacing screen .07,.12

set style line 80 lc rgb "#404040" lt 1 lw 1
set border ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, y2tics ls 81, ls 81, ls 82, ls 81


set style line 1 lc rgb "#e41a1c"
set style line 2 lc rgb "#ff9000"
set style line 3 lc rgb "#4daf4a"
set style line 4 lc rgb "#2d7cff"
set style line 5 lc rgb "#202020" lw 0.5

set key outside top center horizontal maxrows 1 font ',16'
set key samplen 2

set xrange [1:96]
set xtics 16 offset 0,0.5 nomirror scale 0.5
#set mxtics 2
#set xlabel "Frames" offset 0,1

set yrange [0:]
set ytics offset 0.5,0 scale 0
set ylabel "References" offset 1,0

set y2range [1:]
set y2tics offset -0.5,0 scale 0
set logscale y2
#set y2label "Size (in kB)" offset -1,0

frame(t,f)=sprintf("<awk '$2 == \"%s\" {print $0}' %s", t, f)
unset key
unset ylabel
do for [i=1:75] {
IN_FILE=sprintf('%s-%d.dat',PREFIX, i)
plot \
    frame('I', IN_FILE) u 1:($4/1000):(0) axis x1y2 t 'I-frame' w boxes ls 1, \
    frame('P', IN_FILE) u 1:($4/1000):(0) axis x1y2 t 'P-frame' w boxes ls 3, \
    frame('B', IN_FILE) u 1:($4/1000):(0) axis x1y2 t 'B-frame' w boxes ls 4, \
    IN_FILE u 1:($4/1000) axis x1y2 t 'Size' w l ls 5, \
    '' u 1:($3/1000) t 'References'  w l ls 2
}
unset multiplot
unset output
