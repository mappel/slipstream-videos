#!/bin/bash
for vid in *-weights-sizes
do
	echo $vid
	PREFIX=${vid%-weights-sizes}
	gnuplot -e 'PREFIX="'$vid/$PREFIX'"' -e 'OUT_FILE="'$vid/$PREFIX-all.pdf'"' plot-all-segments.gp
done
