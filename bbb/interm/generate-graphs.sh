#!/bin/bash
rm log.txt graphs.txt 2> /dev/null
for vid in *.h264
do
	PREFIX=${vid%.h264}
	DIR="$PREFIX"-graphs
	mkdir -p "$DIR"
	echo "$vid".log >> log.txt
	echo "$DIR"/"$PREFIX" >> graphs.txt
done
parallel --bar /pool/data/mappel/mb-parser/build/mb_parser {1} --dat {2} :::: log.txt ::::+ graphs.txt
rm log.txt graphs.txt
