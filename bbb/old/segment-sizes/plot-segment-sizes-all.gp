set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 1 rounded solid

set style line 80 lc rgb "#202020" lt 1 lw 1
set border 2 back ls 80

set style line 81 lc rgb "#505050" lt 0 lw 0.7
set style line 82 lc rgb "#707070" lt 0 lw 0.5
set grid y,x,mx back ls 81, ls 81, ls 82

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify

set tics out
set xtics scale 0.25
set xtics 15
set mxtics 15
set xrange [0:75]

#set style line 1 lc rgb "#e41a1c" lw 0.5
#set style line 2 lc rgb "#377eb8" lw 0.5
#set style line 3 lc rgb "#4daf4a" lw 0.5
#set style line 4 lc rgb "#984ea3" lw 0.5
#set style line 5 lc rgb "#ff7f00" lw 0.5
#set style line 6 lc rgb "#f61067" lw 0.5
#set style line 7 lc rgb "#6622cc" lw 0.5
#set style line 8 lc rgb "#92b4a7" lw 0.5
#set style line 9 lc rgb "#a5be00" lw 0.5

set style line 1 lc rgb "#e41a1c" 
set style line 2 lc rgb "#377eb8" 
set style line 3 lc rgb "#4daf4a" 
set style line 4 lc rgb "#984ea3" 
set style line 5 lc rgb "#ff7f00" 
set style line 6 lc rgb "#f61067" 
set style line 7 lc rgb "#6622cc" 
set style line 8 lc rgb "#92b4a7" 
set style line 9 lc rgb "#a5be00" 


set key outside top horizontal maxrows 1 font ',16'
set key samplen 2

set xlabel "Segments" offset 0,0.5

#set yrange [0:1]
#set ytics 0,0.2
#set mytics 2
set ytics scale 0
set ylabel "Mbps" offset 2,0
#set logscale y


set output OUT_FILE
plot \
     IN_FILE09 u ($0):($1*8/1000000/4):(0) t '2160p 10k' w filledcurves ls 9,\
     IN_FILE08 u ($0):($1*8/1000000/4):(0) t '1080p 5k8' w filledcurves  ls 1,\
     IN_FILE07 u ($0):($1*8/1000000/4):(0) t '1080p 4k3' w filledcurves  ls 7,\
     IN_FILE06 u ($0):($1*8/1000000/4):(0) t '720p 3k' w filledcurves  ls 3,\
     IN_FILE05 u ($0):($1*8/1000000/4):(0) t '720p 2k35' w filledcurves ls 5 ,\
     IN_FILE04 u ($0):($1*8/1000000/4):(0) t '480p 1k75' w filledcurves ls 4 ,\
     IN_FILE03 u ($0):($1*8/1000000/4):(0) t '480p 1k05' w filledcurves ls 6 ,\
     IN_FILE02 u ($0):($1*8/1000000/4):(0) t '384p 0k56' w filledcurves ls 2 ,\
     IN_FILE01 u ($0):($1*8/1000000/4):(0) t '240p 0k235' w filledcurves ls 8
unset output
