#!/bin/bash
for folder in $1/*-weights-sizes
do
	VIDEO=${folder%\.264-weights-sizes}
	rm $folder/$VIDEO-segment-sizes.dat 2> /dev/null
	for i in $(seq 1 75)
	do
		awk '{s+=$4} END {print s}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-segment-sizes.dat
	done
done
