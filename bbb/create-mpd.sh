#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-bbb.mpd bbb_2160p_10k_24fps_96key_300s_dash.mpd bbb_1440p_7k4_24fps_96key_300s_dash.mpd bbb_1080p_5k8_24fps_96key_300s_dash.mpd bbb_1080p_4k3_24fps_96key_300s_dash.mpd bbb_720p_3k_24fps_96key_300s_dash.mpd bbb_720p_2k35_24fps_96key_300s_dash.mpd bbb_480p_1k75_24fps_96key_300s_dash.mpd bbb_480p_1k05_24fps_96key_300s_dash.mpd bbb_360p_0k75_24fps_96key_300s_dash.mpd bbb_360p_0k56_24fps_96key_300s_dash.mpd bbb_240p_0k375_24fps_96key_300s_dash.mpd bbb_240p_0k235_24fps_96key_300s_dash.mpd bbb_144p_0k16_24fps_96key_300s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-bbb.mpd
../mpd-scripts/make-ful-rel.py slipstream-bbb.mpd reliable-bbb.mpd
mv *_dash.mpd interm
