#!/bin/bash
rm log.txt weights.txt 2> /dev/null
for vid in *.h264
do
	PREFIX=${vid%.h264}
	DIR="$PREFIX"-weights
	mkdir -p "$DIR"
	echo "$vid".log >> log.txt
	echo "$DIR"/"$PREFIX" >> weights.txt
done
parallel --bar /pool/data/mappel/mb-parser/build/mb_parser {1} --summary {2} :::: log.txt ::::+ weights.txt
rm log.txt weights.txt
