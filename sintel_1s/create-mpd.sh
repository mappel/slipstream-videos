#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-sintel_1s.mpd sintel_1s_2160p_10k_24fps_24key_300s_dash.mpd sintel_1s_1440p_7k4_24fps_24key_300s_dash.mpd sintel_1s_1080p_5k8_24fps_24key_300s_dash.mpd sintel_1s_1080p_4k3_24fps_24key_300s_dash.mpd sintel_1s_720p_3k_24fps_24key_300s_dash.mpd sintel_1s_720p_2k35_24fps_24key_300s_dash.mpd sintel_1s_480p_1k75_24fps_24key_300s_dash.mpd sintel_1s_480p_1k05_24fps_24key_300s_dash.mpd sintel_1s_360p_0k75_24fps_24key_300s_dash.mpd sintel_1s_360p_0k56_24fps_24key_300s_dash.mpd sintel_1s_240p_0k375_24fps_24key_300s_dash.mpd sintel_1s_240p_0k235_24fps_24key_300s_dash.mpd sintel_1s_144p_0k16_24fps_24key_300s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-sintel_1s.mpd
../mpd-scripts/make-ful-rel.py slipstream-sintel_1s.mpd reliable-sintel_1s.mpd
mv *_dash.mpd interm
