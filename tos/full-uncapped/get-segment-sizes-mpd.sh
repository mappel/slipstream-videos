#!/bin/bash
for MPD in *.mpd
do
    PREFIX=${MPD%_dash.mpd}
    CSV="$PREFIX"-segment-sizes.csv
    DAT="$PREFIX"-segment-sizes.dat
    ../../mpd-scripts/extract_ss.py "$MPD" "$CSV"
    cat "$CSV" | cut -d',' -f2 > "$DAT"
    sed -i 's/size(byte)/0/' "$DAT"
    rm "$CSV"
done
