#!/bin/bash
for folder in $1/*-weights-sizes
do
	VIDEO=${folder%\.264-weights-sizes}
	rm $folder/$VIDEO-segment-sizes.dat 2> /dev/null
	for i in $(seq 1 75)
	do
		awk '$2 == "I" {s+=1} END {print s}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-I-frames.dat
		awk '$2 == "P" {s+=1} END {print s}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-P-frames.dat
		awk '$2 == "B" {s+=1} END {print s}' $folder/$VIDEO-$i.dat >> $folder/$VIDEO-B-frames.dat
	done
done
