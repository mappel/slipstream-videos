#!/bin/bash
for folder in $1/*-weights-sizes
do
	VIDEO=${folder%\.264-weights-sizes}
	rm $folder/$VIDEO-frames 2> /dev/null
	awk '{s+=$1;n+=$2} END {print "I: "s" "n}' $folder/$VIDEO-I-frames.dat >> $folder/$VIDEO-frames.dat
	awk '{s+=$1;n+=$2} END {print "P: "s" "n}' $folder/$VIDEO-P-frames.dat >> $folder/$VIDEO-frames.dat
	awk '{s+=$1;n+=$2} END {print "B: "s" "n}' $folder/$VIDEO-B-frames.dat >> $folder/$VIDEO-frames.dat
done
