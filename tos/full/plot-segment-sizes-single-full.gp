set terminal pdfcairo enhanced color font "Clear Sans, 20" linewidth 2 rounded solid size 12.3,3

set style line 80 lc rgb "#404040" lt 1 lw 1
set border 3 back ls 80

set style line 81 lc rgb "#606060" lt 0 lw 0.6
set style line 82 lc rgb "#808080" lt 0 lw 0.3
set grid y, x, mxtics, mytics back ls 81, ls 81, ls 82

set xtics border in scale 1,0.5 nomirror norotate autojustify
set ytics border in scale 1,0.5 nomirror norotate autojustify

set style line 1 lc rgb "#984ea3" lw 0.5

unset key

set xrange [0:]
set xtics 0,10 offset 0,0.5
set mxtics 2
set xlabel "Segments" offset 0,1

set yrange [0:]
set ylabel "Throughput (in Mbps)" font ',18' offset 1,0

set object 1 rectangle from 0,0 to 75,25 behind fillcolor rgb "#999999" fillstyle transparent solid 0.5 noborder

set output OUT_FILE
plot \
     IN_FILE u ($0):($1*8/1000000/4):(0) w filledcurves ls 1
unset output
