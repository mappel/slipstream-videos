#!/bin/bash
mkdir -p segment-sizes
for folder in *-weights-sizes
do
	VIDEO=${folder%-weights-sizes}
	rm segment-sizes/$VIDEO-segment-sizes.dat 2> /dev/null
	echo 0 >> segment-sizes/$VIDEO-segment-sizes.dat
	for i in $(seq 1 75)
	do
		awk '{s+=$4} END {print s}' $folder/$VIDEO-$i.dat >> segment-sizes/$VIDEO-segment-sizes.dat
	done
done
