#!/bin/bash
if [ $# -lt 2 ]
then
    echo "usage: $0 <input suffix> <threshold> [<output suffix>]"
    exit
fi
INPUT_SUFFIX="$1"
THRESHOLD="$2"
if [ $# -eq 3 ]
then
    OUTPUT_SUFFIX="$3"
else
    OUTPUT_SUFFIX=""
fi
rm log.txt chains.txt params.txt 2> /dev/null
for GRAPH_DIR in *-"$INPUT_SUFFIX"
do
    PREFIX=${GRAPH_DIR%-$INPUT_SUFFIX}
    if [ "$OUTPUT_SUFFIX" == "" ]
    then
        DIR="$PREFIX"-chain-weights-"$THRESHOLD"p
    else
        DIR="$PREFIX"-chain-weights-"$THRESHOLD"p-"$OUTPUT_SUFFIX"
    fi
    RESOLUTION=$(echo "$DIR" | grep -o '[0-9]\{3,\}p')
    RESOLUTION=${RESOLUTION%p}
    mkdir -p "$DIR"
    for SEGMENT in "$GRAPH_DIR"/*.dat
    do
        SEGMENT_NAME=$(basename $SEGMENT)
        echo "$SEGMENT" >> log.txt
        echo "$DIR"/"$SEGMENT_NAME" >> chains.txt
        echo "$RESOLUTION" >> params.txt
    done
done
parallel /pool/data/mappel/mb-parser/build/chain_weights {1} {2} "$THRESHOLD" --absolute {3} :::: log.txt ::::+ chains.txt ::::+ params.txt
rm log.txt chains.txt params.txt
