#!/bin/bash
./generate-logs.sh
./generate-graphs.sh
./generate-ranges.sh
./get-filtered-graphs.sh $1
./generate-chain-weights.sh graphs 1
./generate-chain-weights.sh graphs-$1f-chunks 1 $1f-chunks
