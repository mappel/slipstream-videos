#!/bin/bash
MPD=slipstream-chain-1p-collegehumor_long.mpd
mv "$MPD" interm
cd interm
for vid in *dashinit.mp4
do
	PREFIX=${vid%_dashinit.mp4}
	/pool/data/mappel/header_parser/build/header_parser "$vid" --mpd "$MPD" --weights "$PREFIX"-chain-weights-1p/"$PREFIX"
done
mv "$MPD" ..
