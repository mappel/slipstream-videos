#!/usr/bin/python3
import sys
import os

FILE_ENDING = '.dat'

def get_input_files(input_dir: str, prefix: str):
    it = os.scandir(input_dir)
    ret = dict()
    for entry in it:
        if entry.is_file() and entry.name.startswith(prefix):
            file_path = input_dir + entry.name
            segment_no = entry.name[len(prefix) + 1:-len(FILE_ENDING)]
            if segment_no in ret:
                raise ValueError('Duplicate segment number: ' + segment_no)
            ret[segment_no] = file_path
    return ret


def process_input_file(file_path: str):
    lines = list()
    with open(file_path, 'r') as f:
        lines = f.readlines()
    last_assigned_weight = len(lines)
    for idx, line in enumerate(lines):
        line_split = line.split()
        if len(line_split) != 2:
            print('Error: Invalid line (' + str(idx + 1) + '): ' + line.strip(), file=sys.stderr)
            print('in file: ' + file_path, file=sys.stderr)
            continue
        weight = int(line_split[1])
        if weight > 0:
            if last_assigned_weight < 0:
                raise ValueError('Assigning weight smaller than 0. Should not happen!!')
            line_split[1] = str(last_assigned_weight)
            last_assigned_weight -= 1
            lines[idx] = ' '.join(line_split) + '\n'
    return lines


def generate_output_files(input_files: dict, output_dir: str, prefix: str):
    os.makedirs(output_dir, exist_ok=True)
    for segment_no in input_files:
        lines = process_input_file(input_files[segment_no])
        output_file = output_dir + prefix + '-' + segment_no + FILE_ENDING
        with open(output_file, 'w') as f:
            f.writelines(lines)


if __name__ == "__main__":
    if len(sys.argv) != 4:
        print('usage: ' + sys.argv[0] + ' <path/to/input> <path/to/output> <prefix>')
        exit(1)
    input_dir = sys.argv[1]
    if not input_dir.endswith('/'):
        input_dir += '/'
    output_dir = sys.argv[2]
    if not output_dir.endswith('/'):
        output_dir += '/'
    prefix = sys.argv[3]
    if not os.path.exists(input_dir):
        print('Error: Input directory does not exist: ' + input_dir, file=sys.stderr)
        exit(1)
    input_files = get_input_files(input_dir, prefix)
    generate_output_files(input_files, output_dir, prefix)
