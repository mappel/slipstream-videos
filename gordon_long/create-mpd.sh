#!/bin/bash
../mpd-scripts/merge-mpds.py slipstream-gordon_long.mpd gordon_long_2160p_10k_24fps_96key_296s_dash.mpd gordon_long_1440p_7k4_24fps_96key_296s_dash.mpd gordon_long_1080p_5k8_24fps_96key_296s_dash.mpd gordon_long_1080p_4k3_24fps_96key_296s_dash.mpd gordon_long_720p_3k_24fps_96key_296s_dash.mpd gordon_long_720p_2k35_24fps_96key_296s_dash.mpd gordon_long_480p_1k75_24fps_96key_296s_dash.mpd gordon_long_480p_1k05_24fps_96key_296s_dash.mpd gordon_long_360p_0k75_24fps_96key_296s_dash.mpd gordon_long_360p_0k56_24fps_96key_296s_dash.mpd gordon_long_240p_0k375_24fps_96key_296s_dash.mpd gordon_long_240p_0k235_24fps_96key_296s_dash.mpd gordon_long_144p_0k16_24fps_96key_296s_dash.mpd
sed -i 's/ indexRange="[0-9]*-[0-9]*"//' slipstream-gordon_long.mpd
../mpd-scripts/make-ful-rel.py slipstream-gordon_long.mpd reliable-gordon_long.mpd
mv *_dash.mpd interm
