#!/bin/bash
set -eou pipefail
CPU=$(nproc)
wget ftp://ftp.videolan.org/pub/x264/snapshots/x264-snapshot-20190925-2245-stable.tar.bz2
tar -xvf x264-snapshot-20190925-2245-stable.tar.bz2
cd x264-snapshot-20190925-2245-stable
./configure --prefix=$PWD --enable-pic --enable-static
make -j "$CPU"
make install
