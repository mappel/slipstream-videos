#!/bin/bash
set -eou pipefail
# usage: ./build-ffmpeg <path/to/vmaf> <path/to/x264>
VMAF="${1%/}"/lib/pkgconfig
X264="${2%/}"/lib/pkgconfig
CPU=$(nproc)
wget https://github.com/FFmpeg/FFmpeg/archive/n4.1.3.tar.gz
tar -xzvf n4.1.3.tar.gz
cd FFmpeg-n4.1.3
export PKG_CONFIG_PATH="$VMAF":"$X264":"$PKG_CONFIG_PATH"
./configure --prefix=$PWD --enable-libx264 --enable-gpl --enable-libvmaf --enable-version3
make -j "$CPU"
make install
