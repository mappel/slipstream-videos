#!/bin/bash
set -euo pipefail
CPU=$(nproc)
wget https://github.com/Netflix/vmaf/archive/v1.3.15.tar.gz
tar -xzvf v1.3.15.tar.gz
cd vmaf-1.3.15
ROOT=$PWD
cd ptools
make -j "$CPU"
cd ../wrapper
sed -i "s#/usr/local#$ROOT#" libvmaf.pc
sed -i "s#INSTALL_PREFIX = /usr/local#INSTALL_PREFIX = $ROOT#" Makefile
make -j "$CPU"
make install
