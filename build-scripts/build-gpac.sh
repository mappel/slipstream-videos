#!/bin/bash
set -eou pipefail
CPU=$(nproc)
wget https://github.com/gpac/gpac/archive/v0.7.1.tar.gz
tar -xzvf v0.7.1.tar.gz
cd gpac-0.7.1
./configure --prefix=$PWD --static-mp4box
make -j "$CPU"
make install
