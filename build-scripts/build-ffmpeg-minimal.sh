#!/bin/bash
set -eou pipefail
CPU=$(nproc)
wget https://github.com/FFmpeg/FFmpeg/archive/n4.1.3.tar.gz
tar -xzvf n4.1.3.tar.gz
cd FFmpeg-n4.1.3
./configure --prefix=$PWD
make -j "$CPU"
make install
