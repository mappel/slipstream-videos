#!/bin/bash
set -euxo pipefail
if [ ! $# -eq 7 ]
then
	echo "usage: $0 <source-video> <output-path> <outname-prefix> <width> <height> <bitrate> <bitrate-name>"
	exit 1
fi
SOURCE_VIDEO="$1"
OUTPUT_PATH="${2%/}"
OUTNAME_PREFIX="$3"
OUTRES_W="$4"
OUTRES_H="$5"
BITRATE="$6"
BR_SHORT="$7"

FFMPEG=/pool/data/mappel/FFmpeg-n4.1.3/bin/ffmpeg
MP4BOX=/pool/data/mappel/gpac-0.7.1/bin/MP4Box
SEGMENT_LENGTH_MS=4000
FPS=24
OUTLENGTH_S=300

KEYINT=$((SEGMENT_LENGTH_MS * FPS / 1000))
MAXRATE=$((BITRATE * 2))
BUFSIZE=$((MAXRATE * 2))
OUTNAME="$OUTNAME_PREFIX"_"$OUTRES_H"p_"$BR_SHORT"_"$FPS"fps_"$KEYINT"key_"$OUTLENGTH_S"s

mkdir -p "$OUTPUT_PATH"
WD=$PWD
cd "$OUTPUT_PATH"

"$FFMPEG" -y -i "$SOURCE_VIDEO" -vf "scale="$OUTRES_W":"$OUTRES_H":force_original_aspect_ratio=decrease,setsar=1:1,pad="$OUTRES_W":"$OUTRES_H":(out_w-iw)/2:(out_h-ih)/2" -map 0:0 -c:v libx264 -preset slow -b:v "$BITRATE"k -maxrate "$MAXRATE"k -bufsize "$BUFSIZE"k -g "$KEYINT" -keyint_min "$KEYINT" -sc_threshold 0 -pass 1 -passlogfile "$OUTNAME" -r "$FPS" -t "$OUTLENGTH_S" -f h264 /dev/null
"$FFMPEG" -i "$SOURCE_VIDEO" -vf "scale="$OUTRES_W":"$OUTRES_H":force_original_aspect_ratio=decrease,setsar=1:1,pad="$OUTRES_W":"$OUTRES_H":(out_w-iw)/2:(out_h-ih)/2" -map 0:0 -c:v libx264 -preset slow -b:v "$BITRATE"k -maxrate "$MAXRATE"k -bufsize "$BUFSIZE"k -g "$KEYINT" -keyint_min "$KEYINT" -sc_threshold 0 -pass 2 -passlogfile "$OUTNAME" -r "$FPS" -t "$OUTLENGTH_S" "$OUTNAME".h264
"$MP4BOX" -add "$OUTNAME".h264 -fps "$FPS" "$OUTNAME".mp4
"$MP4BOX" -dash "$SEGMENT_LENGTH_MS" -frag "$SEGMENT_LENGTH_MS" -rap "$OUTNAME".mp4
"$WD"/../add-header.sh "$OUTPUT_PATH"/"$OUTNAME"_dashinit.mp4
mkdir -p interm
mv "$OUTNAME".h264 "$OUTNAME".mp4 "$OUTNAME"_dashinit.mp4 interm
