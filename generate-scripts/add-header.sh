#!/bin/bash

head=$'HTTP/1.1 200 OK\r
Cache-Control: max-age=604800\r
Content-Type: text/html\r
Date: Fri, 06 Jul 2018 15:14:25 GMT\r
Etag: "1541025663+ident"\r
Expires: Fri, 13 Jul 2018 15:14:25 GMT\r
Last-Modified: Fri, 09 Aug 2013 23:54:35 GMT\r
Server: ECS (dca/2473)\r
Vary: Accept-Encoding\r
Access-Control-Allow-Origin: *\r
X-Cache: HIT\r'

length=$(du -b "$1" | cut -f1)

head="${head}"$'\n'"Content-Length: ${length}"$'\n'""
file="${1::-4}_with_http_header.mp4"

echo "$head" > $file
cat "$1" >> $file
	
