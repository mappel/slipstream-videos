#!/usr/bin/python3
from collections import namedtuple
import os
import sys

INPUT_SUFFIX = '-ranges.csv'
OUTPUT_SUFFIX = '-chunks.csv'

Line = namedtuple('Line', 'category type start end')


def parse_line(line: str):
    line_split = line.strip().split(',')
    if len(line_split) != 4:
        print('Error: Invalid line:', line.strip(), file=sys.stderr)
        return None
    return Line(line_split[0], line_split[1], line_split[2], line_split[3])


def process_file(input_path: str, input_file: str, output_path: str):
    lines = list()
    with open(input_path + input_file, 'r') as f:
        lines = f.readlines()
    line_idx = 0
    first_segment_found = False
    # Search for first segment
    while True:
        if len(lines) <= line_idx:
            print('Error: Could not find segment header', file=sys.stderr)
            print('in file:', input_file, file=sys.stderr)
            return
        parsed_line = parse_line(lines[line_idx])
        if parsed_line is None:
            return
        if parsed_line.type == 'sidx':
            break
        line_idx += 1
    # Parse ranges
    output_lines = ['segment_no,chunk_no,start,end\n']
    segment_count = 0
    chunk_count = -1
    chunk_start = -1
    chunk_end = -1
    while True:
        if len(lines) <= line_idx:
            break
        parsed_line = parse_line(lines[line_idx])
        if parsed_line is None:
            return
        # First chunk includes segment header
        if parsed_line.type == 'sidx':
            segment_count += 1
            chunk_count = 1
            chunk_start = parsed_line.start
            if len(lines) <= line_idx + 2:
                print('Error: sidx without moof and mdat in line:', line_idx + 1, file=sys.stderr)
                print('in file:', input_file, file=sys.stderr)
                return
            end_line = parse_line(lines[line_idx + 2])
            if end_line.type != 'mdat':
                print('Error: Expected mdat in line:', line_idx + 1, file=sys.stderr)
                print('in file:', input_file, file=sys.stderr)
                return
            chunk_end = end_line.end
            output_line = [segment_count, chunk_count, chunk_start, chunk_end]
            output_lines.append(','.join(map(str, output_line)) + '\n')
            line_idx += 3
        # Other chunks
        elif parsed_line.type == 'moof':
            chunk_count += 1
            chunk_start = parsed_line.start
            if len(lines) <= line_idx + 1:
                print('Error: moof without mdat in line:', line_idx + 1, file=sys.stderr)
                print('in file:', input_file, file=sys.stderr)
                return
            end_line = parse_line(lines[line_idx + 1])
            if end_line.type != 'mdat':
                print('Error: Expected mdat in line:', line_idx + 1, file=sys.stderr)
                print('in file:', input_file, file=sys.stderr)
                return
            chunk_end = end_line.end
            output_line = [segment_count, chunk_count, chunk_start, chunk_end]
            output_lines.append(','.join(map(str, output_line)) + '\n')
            line_idx += 2
        else:
            print('Segments:', segment_count)
            break
    output_file = input_file[:-len(INPUT_SUFFIX)] + OUTPUT_SUFFIX
    with open(output_path + output_file, 'w') as f:
        f.writelines(output_lines)


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage:', sys.argv[0], '<path/to/input> <path/to/output>')
        exit(1)
    input_path = sys.argv[1]
    if not input_path.endswith('/'):
        input_path += '/'
    output_path = sys.argv[2]
    if not output_path.endswith('/'):
        output_path += '/'
    it = os.scandir(input_path)
    for entry in it:
        if entry.is_file() and entry.name.endswith(INPUT_SUFFIX):
            print(entry.name)
            process_file(input_path, entry.name, output_path)

