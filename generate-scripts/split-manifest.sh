#!/bin/bash

if [ -z "$1" ]
then
	echo "usage: $0 <MPD file>"
        echo "The video needs to be in the same folder and needs to have the format MPD file name + init.mp4."
	echo "Example: bbb_240p_0k235_24f_96sc_300s_dash.mpd -> bbb_240p_0k235_24f_96sc_300s_dashinit.mp4"
exit
fi
VIDEO=${1%.mpd}
mkdir -p $VIDEO
INIT_SEGMENT="$(cat $VIDEO.mpd | grep -o -e 'range="[0-9]*-[0-9]*' | grep -o -e '[0-9]*-[0-9]*')"
CHUNKS="$(cat $VIDEO.mpd | grep -o -e 'mediaRange="[0-9]*-[0-9]*' | grep -o -e '[0-9]*-[0-9]*')"
INIT_START="$(echo $INIT_SEGMENT | cut -d '-' -f1)"
INIT_END="$(echo $INIT_SEGMENT | cut -d '-' -f2)"
dd if="$VIDEO"init.mp4 ibs=1 skip=$INIT_START count=$((INIT_END-INIT_START+1)) of="$VIDEO/0_$VIDEO.mp4"
count=1

for c  in $CHUNKS
do
	CHUNK_START="$(echo $c | cut -d '-' -f1)"
	CHUNK_END="$(echo $c | cut -d '-' -f2)"
	dd if="$VIDEO"init.mp4 iflag=skip_bytes,count_bytes skip=$CHUNK_START count=$((CHUNK_END-CHUNK_START+1)) of="$VIDEO/$count"_"$VIDEO.mp4"
	count=$((count+1))
done
