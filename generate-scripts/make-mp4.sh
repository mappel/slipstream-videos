#!/bin/bash
set -euxo pipefail
if [ ! $# -eq 1 ]
then
	echo "usage: $0 <video>"
	exit 1
fi

OUTNAME=${1%.h264}
MP4BOX=/pool/data/mappel/gpac-0.7.1/bin/MP4Box
HEADER=/pool/data/mappel/videos/generate-scripts/add-header.sh
SEGMENT_LENGTH_MS=4000
FPS=24

"$MP4BOX" -add "$OUTNAME".h264 -fps "$FPS" "$OUTNAME".mp4
"$MP4BOX" -dash "$SEGMENT_LENGTH_MS" -frag "$SEGMENT_LENGTH_MS" -rap "$OUTNAME".mp4
"$HEADER" "$OUTNAME"_dashinit.mp4
