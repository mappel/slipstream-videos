#!/usr/bin/python3
import sys
import re

if len(sys.argv) != 4:
    print('usage: ' + sys.argv[0] + ' <MPD file> <output> <segment-len-in-ms>')
    exit(1)
mpd = sys.argv[1]
out = sys.argv[2]
segment_length = int(sys.argv[3])
media_range_match = re.compile('\\d+')
segment_count = 0
segment_sizes = list()
with open(mpd, 'r') as f:
    for line in f:
        if 'mediaRange' not in line:
            continue
        segment_count += 1
        line_split = line.split()
        if len(line_split) != 3:
            print('Error: Unexpected line split length: ' + str(len(line_split)) + ' (expected 3)', file=sys.stderr)
            print('line: ' + line.strip(), file=sys.stderr)
            segment_sizes.append((segment_count, -1))
            continue
        media_range_str = line_split[1]
        media_range = media_range_match.findall(media_range_str)
        if media_range is None or len(media_range) != 2:
            print('Error: Failed to match media range in string: ' + media_range_str, file=sys.stderr)
            segment_sizes.append((segment_count, -1))
            continue
        try:
            # + 1 because ranges are zero-indexed, i.e., 0-851 are 852 bytes
            segment_size = int(media_range[1]) - int(media_range[0]) + 1
        except ValueError as e:
            print('Error: Failed to calculate segment size: ' + str(e), file=sys.stderr)
            segment_sizes.append((segment_count, -1))
            continue
        segment_sizes.append((segment_count, segment_size))
with open(out, 'w') as f:
    f.write('segment_no size(byte) bitrate(kbps)\n')
    for entry in segment_sizes:
        line = [entry[0]]
        line.append(entry[1])
        # bit / ms == kbit / s
        line.append(entry[1] * 8 / segment_length)
        f.write(' '.join(map(str, line)) + '\n')
