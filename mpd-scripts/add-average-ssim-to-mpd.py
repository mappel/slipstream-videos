#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os
import sqlite3
import sys


def get_arbitrary_order(db_cursor: sqlite3.Cursor, video: str) -> str:
    """Return an arbitrary order that exists for the specified video for zero
    dropped frames."""
    stmt = 'SELECT frame_order FROM data WHERE video = ? AND frames_dropped = 0 LIMIT 1'
    res = db_cursor.execute(stmt, (video,)).fetchone()
    if res is None:
        print('Error: No valid entry found for video:', video, file=sys.stderr)
        return None
    return res[0]


def get_average_ssims(db_cursor: sqlite3.Cursor, video: str, order: str) -> list:
    """Return a list of average SSIMs of each quality of the specified video in
    descending quality order"""
    stmt = 'SELECT quality, AVG(ssim) FROM data WHERE video = ? AND frame_order = ? AND frames_dropped = 0 GROUP BY quality ORDER BY quality ASC'
    ret = list()
    prev_quality = -1
    for row in db_cursor.execute(stmt, (video, order)):
        if prev_quality + 1 != row[0]:
            print('Error: Gap in quality levels. Expected quality', prev_quality + 1, 'got', row[0], file=sys.stderr)
            print('       Can not continue since we add to the MPD in an implicitly decreasing quality order and this breaks.', file=sys.stderr)
            return None
        prev_quality = row[0]
        ret.append(row[1])
    ret.reverse()
    return ret


def add_avg_ssim(line: str, ssim: float) -> str:
    """Add an avgSSIM attribute with the specified SSIM to the line.
    Checks for existing attributes and replaces them."""
    # I'd rather use f'avgSSIM="{ssim}"' but rechenknecht's python is from the
    # stone ages...
    avg_ssim_string = 'avgSSIM="{ssim}"'.format(ssim=ssim)
    indent_size = len(line) - len(line.lstrip())
    line_split = line.split()
    avg_ssim_pos = -1
    for idx, attribute in enumerate(line_split):
        attribute_split = attribute.split('=')
        if len(attribute_split) != 2:
            continue
        if attribute_split[0] == 'avgSSIM':
            avg_ssim_pos = idx
            break
    if avg_ssim_pos != -1:
        print('Warning: Overwriting existing avgSSIM attribute for representation:', line.strip(), file=sys.stderr)
        line_split[avg_ssim_pos] = avg_ssim_string
    elif len(line_split) < 3:
        print('Warning: Representation tag is suspiciously small. Inserting after first element, but format should be checked:', line.strip(), file=sys.stderr)
        line_split.insert(1, avg_ssim_string)
    else:
        # This should be after the id attribute.
        line_split.insert(2, avg_ssim_string)
    return ' ' * indent_size + ' '.join(line_split) + '\n'


def modify_mpd(avg_ssims: list, input_mpd: str, output_mpd: str) -> bool:
    """Add an avgSSIM attribute to each <Representation> of the MPD.
    Size of avg_ssims must match the number of representations in the MPD.
    Since avg_ssims is in descending quality order, the same order is assumed
    for representations in the MPD."""
    error = False
    idx = 0
    with open(input_mpd, 'r') as i, open(output_mpd, 'w') as o:
        for line in i:
            if '<Representation' in line:
                if idx + 1 > len(avg_ssims):
                    print('Error: More representation tags than average SSIM values in database.', file=sys.stderr)
                    error = True
                    break
                line = add_avg_ssim(line, avg_ssims[idx])
                idx += 1
            o.write(line)
    if idx != len(avg_ssims):
        print('Error: Could not assign all SSIM values. Representations:', idx, 'SSIMs:', len(avg_ssims), file=sys.stderr)
        error = True
    if error is True and os.path.exists(output_mpd):
        os.remove(output_mpd)
    return error


if __name__ == '__main__':
    if len(sys.argv) != 5:
        print('usage:', sys.argv[0], '<ssim-db> <video> <input-MPD> <output-MPD>')
        exit(1)
    database = sys.argv[1]
    video = sys.argv[2]
    mpd = sys.argv[3]
    output = sys.argv[4]

    if mpd == output:
        print('Error: Output file must be different from input file.', file=sys.stderr)
        exit(1)

    db_connection = sqlite3.connect(database)
    db_cursor = db_connection.cursor()
    order = get_arbitrary_order(db_cursor, video)
    if order is None:
        exit(1)
    avg_ssims = get_average_ssims(db_cursor, video, order)
    if avg_ssims is None:
        exit(1)
    if modify_mpd(avg_ssims, mpd, output):
        exit(1)
    exit(0)


