#!/usr/bin/python3
import sys
from collections import namedtuple

class Range:
    def __init__(self, start: int, end: int):
        self.start = start
        self.end = end
        if (start > end):
            print('Error: Constructing invalid range: ' + str(self), file=sys.stderr)

    def size(self):
        return max(self.end - self.start + 1, 0)

    def __lt__(self, other):
        return self.start < other.start

    def __str__(self):
        return str(self.start) + '-' + str(self.end)


def get_id_and_bandwidth_from_representation(line: str):
    id = str()
    bandwidth = str()
    line_split = line.split()
    for attribute in line_split:
        attribute_split = attribute.split('=')
        if len(attribute_split) != 2:
            continue
        value = attribute_split[1].strip('">/')
        if attribute_split[0] == 'id':
            id = value
        elif attribute_split[0] == 'bandwidth':
            bandwidth = value
    if id == '' or bandwidth == '':
        print('Error: Failed to find id or bandwidth attribute in Representation line: ' + line, file=sys.stderr)
        return '', ''
    return id, bandwidth

def check_ranges(ranges_list: list):
    if len(ranges_list) == 0:
        return
    prev_range = ranges_list[0]
    for i in range(1, len(ranges_list)):
        curr_range = ranges_list[i]
        if curr_range.start != prev_range.end + 1:
            print('Error: Gap between ranges ' + str(prev_range) + ' and ' + str(curr_range), file=sys.stderr)
        prev_range = curr_range


def parse_range(range: str):
    range_split = range.split('-')
    if len(range_split) != 2:
        print('Error: Invalid range: ' + range, file=sys.stderr)
        return None
    return Range(int(range_split[0]), int(range_split[1]))


def parse_ranges(ranges: str):
    ranges_list = list()
    ranges_size = 0
    ranges_split = ranges.split(',')
    for range in ranges_split:
        parsed_range = parse_range(range)
        if parsed_range is None:
            continue
        ranges_list.append(parsed_range)
        ranges_size += parsed_range.size()
    return ranges_list, ranges_size


def parse_threshold(threshold: str):
    threshold_split = threshold.split(':')
    if len(threshold_split) != 3:
        print('Error: Invalid threshold: ' + threshold, file=sys.stderr)
        return None
    return threshold_split


def parse_tresholds(thresholds: str):
    thresholds_list = list()
    thresholds_split = thresholds.split(',')
    for threshold in thresholds_split:
        parsed_threshold = parse_threshold(threshold)
        if parsed_threshold is None:
            continue
        thresholds_list.append(parsed_threshold)
    return thresholds_list


def check_threshold(threshold: list, unreliable_ranges: list):
    if int(threshold[1]) > len(unreliable_ranges):
        print('Error: Frame threshold is larger than number of ranges: ' + str(thresholds) + ' ' + str(unreliable_ranges), file=sys.stderr)
        return
    range_size = 0
    for i in range(int(threshold[1])):
        range_size += unreliable_ranges[i].size()
    if int(threshold[2]) != range_size:
        print('Error: Size threshold does not match ranges: ' + str(thresholds) + ' ' + str(unreliable_ranges), file=sys.stderr)


def check_segment_url(line: str):
    media_range = None
    reliable_size_ranges = None
    unreliable_size_ranges = None
    reliable_size_attribute = None
    thresholds_list = None
    line_split = line.split()
    ranges = list()
    for attribute in line_split:
        attribute_split = attribute.split('=')
        if len(attribute_split) != 2:
            continue
        value = attribute_split[1].strip('"/>')
        if attribute_split[0] == 'mediaRange':
            media_range = parse_range(value)
        elif attribute_split[0] == 'reliable':
            reliable_ranges, reliable_size_ranges = parse_ranges(value)
            ranges += reliable_ranges
        elif attribute_split[0] == 'unreliable':
            unreliable_ranges, unreliable_size_ranges = parse_ranges(value)
            ranges += unreliable_ranges
        elif attribute_split[0] == 'reliableSize':
            reliable_size_attribute = int(value)
        elif attribute_split[0] == 'thresholds':
            thresholds_list = parse_tresholds(value)

    attribute_missing = False
    if media_range is None:
        print('Error: mediaRange attribute missing: ' + line, file=sys.stderr)
        attribute_missing = True
    if reliable_size_ranges is None:
        print('Error: reliable attribute missing: ' + line, file=sys.stderr)
        attribute_missing = True
    if reliable_size_attribute is None:
        print('Error: reliableSize attribute missing: ' + line, file=sys.stderr)
        attribute_missing = True
    if attribute_missing:
        return
    ranges.sort()
    check_ranges(ranges)
    if reliable_size_ranges != reliable_size_attribute:
        print('Error: The reliable ranges size does not match the reliableSize attribute. ranges size: '
                + str(reliable_size_ranges) + ' reliableSize: ' + str(reliable_size_attribute), file=sys.stderr)
    segment_size_ranges = reliable_size_ranges
    if unreliable_size_ranges is not None:
        segment_size_ranges += unreliable_size_ranges
    if segment_size_ranges != media_range.size():
        print('Error: The size of the specified ranges is not equal to the mediaRange size. ranges size: '
                + str(segment_size_ranges) + ' mediaRange: ' + str(media_range.size()), file=sys.stderr)
    first_range_start = ranges[0].start
    if media_range.start != first_range_start:
        print('Error: mediaRange start is not equal to first range start. mediaRange: ' + str(media_range.start)
                + ' range: ' + str(first_range_start), file=sys.stderr)
    last_range_end = ranges[-1].end
    if media_range.end != last_range_end:
        print('Error: mediaRange end is not equal to first range end. mediaRange: ' + str(media_range.end)
                + ' range: ' + str(last_range_end), file=sys.stderr)
    if thresholds_list is not None:
        for threshold in thresholds_list:
            check_threshold(threshold, unreliable_ranges)
    return media_range


def check_mpd(mpd_file: str):
    representations = dict()
    last_media_range = None
    with open(mpd_file, 'r') as f:
        for line in f:
            if '<Representation' in line:
                curr_id, curr_bandwidth = get_id_and_bandwidth_from_representation(line.strip())
                if curr_id in representations:
                    print('Error: Duplicate representation id: ' + curr_id, file=sys.stderr)
                else:
                    representations[curr_id] = curr_bandwidth
                    print('representation: id=' + curr_id + ' bandwidth=' + curr_bandwidth)
                last_media_range = None
            elif 'SegmentURL' in line:
                media_range = check_segment_url(line.strip())
                if last_media_range is not None and last_media_range.end + 1 != media_range.start:
                    print('Error: Gap in mediaRange attributes at SegmentURL: ' + line.strip(), file=sys.stderr)
                last_media_range = media_range


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print('usage: ' + sys.argv[0] + ' <mpd>')
        exit(1)
    mpd_file = sys.argv[1]
    check_mpd(mpd_file)

