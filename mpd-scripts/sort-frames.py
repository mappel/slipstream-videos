#!/usr/bin/python3
import sys
import os


def get_frames_position(line_split: list):
    current_position = 0
    for entry in line_split:
        if entry.startswith('unreliable'):
            return current_position
        current_position += 1
    return -1


def sort_frame_ranges(frame_ranges: str):
    frame_ranges = frame_ranges.lstrip('unreliable="').rstrip('"/>').split(',')
    frame_list = list()
    for range in frame_ranges:
        range_split = range.split('-')
        if len(range_split) != 2:
            raise ValueError('Invalid range: ' + range)
        frame_list.append((int(range_split[0]), int(range_split[1])))
    frame_list.sort()
    sorted_frame_ranges = ''
    for start, end in frame_list:
        sorted_frame_ranges += str(start) + '-' + str(end) + ','
    return 'unreliable="' + sorted_frame_ranges[:-1] + '"'

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print('usage: ' + sys.argv[0] + ' <input-mpd>')
        exit(1)
    input_mpd = sys.argv[1]
    current_segment_threshold_file = None
    outname = os.path.splitext(input_mpd)[0] + '-sorted.mpd'
    outfile = open(outname, 'w')
    with open(input_mpd, 'r') as f:
        for line in f:
            if 'SegmentURL' in line:
                line_split = line.strip().split()
                frames_position = get_frames_position(line_split)
                if frames_position < 0:
                    raise ValueError('Failed to find \'frames\' attribute in line: ' + line.strip())
                frame_ranges = line_split[frames_position]
                line_split[frames_position] = sort_frame_ranges(frame_ranges)
                outfile.write('          ' + ' '.join(line_split) + '\n')
            else:
                outfile.write(line)
    outfile.close()
    exit(0)
