#!/bin/bash
SCRIPT=/pool/data/mappel/videos/mpd-scripts/generate-mpd-from-thresholds.py
ADD_HEADERS=/pool/data/mappel/videos/generate-scripts/add-header.sh

"$SCRIPT" configs/bbb-0.001-1.cfg no-header/slipstream-0.001-1-bbb.mpd
"$SCRIPT" configs/bbb-0.001-2.cfg no-header/slipstream-0.001-2-bbb.mpd
"$SCRIPT" configs/bbb-0.001-4.cfg no-header/slipstream-0.001-4-bbb.mpd
"$SCRIPT" configs/bbb-0.001-8.cfg no-header/slipstream-0.001-8-bbb.mpd
"$SCRIPT" configs/bbb-0.001-16.cfg no-header/slipstream-0.001-16-bbb.mpd
"$SCRIPT" configs/bbb-0.001-32.cfg no-header/slipstream-0.001-32-bbb.mpd
"$SCRIPT" configs/bbb-0.001-48.cfg no-header/slipstream-0.001-48-bbb.mpd
"$SCRIPT" configs/ed-0.001-1.cfg no-header/slipstream-0.001-1-ed.mpd
"$SCRIPT" configs/sintel-0.001-1.cfg no-header/slipstream-0.001-1-sintel.mpd
"$SCRIPT" configs/tos-0.001-1.cfg no-header/slipstream-0.001-1-tos.mpd

"$ADD_HEADERS" no-header/slipstream-0.001-1-bbb.mpd
mv no-header/slipstream-0.001-1-bbb_with_http_header.mp4 with-header/slipstream-0.001-1-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-2-bbb.mpd
mv no-header/slipstream-0.001-2-bbb_with_http_header.mp4 with-header/slipstream-0.001-2-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-4-bbb.mpd
mv no-header/slipstream-0.001-4-bbb_with_http_header.mp4 with-header/slipstream-0.001-4-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-8-bbb.mpd
mv no-header/slipstream-0.001-8-bbb_with_http_header.mp4 with-header/slipstream-0.001-8-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-16-bbb.mpd
mv no-header/slipstream-0.001-16-bbb_with_http_header.mp4 with-header/slipstream-0.001-16-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-32-bbb.mpd
mv no-header/slipstream-0.001-32-bbb_with_http_header.mp4 with-header/slipstream-0.001-32-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-48-bbb.mpd
mv no-header/slipstream-0.001-48-bbb_with_http_header.mp4 with-header/slipstream-0.001-48-bbb.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-1-ed.mpd
mv no-header/slipstream-0.001-1-ed_with_http_header.mp4 with-header/slipstream-0.001-1-ed.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-1-sintel.mpd
mv no-header/slipstream-0.001-1-sintel_with_http_header.mp4 with-header/slipstream-0.001-1-sintel.mpd
"$ADD_HEADERS" no-header/slipstream-0.001-1-tos.mpd
mv no-header/slipstream-0.001-1-tos_with_http_header.mp4 with-header/slipstream-0.001-1-tos.mpd
