#!/bin/bash
set -euo pipefail
VIDEOS=(brooklyn_long collegehumor_long dude_long faze_long gordon_long katy_long tana_long turks_long unbox_long waseda_long)
SSIM_DIFF=0.001
NTH_FRAME=1
for VIDEO in ${VIDEOS[*]}
do
    sed "s/<VIDEO>/${VIDEO}/g;s/<DIFF>/${SSIM_DIFF}/;s/<NTH>/${NTH_FRAME}/" \
        template.cfg > "${VIDEO}-${SSIM_DIFF}-${NTH_FRAME}.cfg"
done
