#!/bin/bash
set -euo pipefail

SCRIPT=/pool/data/mappel/videos/mpd-scripts/generate-mpd-from-thresholds.py
ADD_HEADERS=/pool/data/mappel/videos/generate-scripts/add-header.sh

VIDEOS=(brooklyn_long collegehumor_long dude_long faze_long gordon_long katy_long tana_long turks_long unbox_long waseda_long)
for VIDEO in ${VIDEOS[*]}
do
    echo "${VIDEO}"
    "${SCRIPT}" "configs/${VIDEO}-0.001-1.cfg" \
        "no-header/slipstream-0.001-1-${VIDEO}.mpd"
    "${ADD_HEADERS}" "no-header/slipstream-0.001-1-${VIDEO}.mpd"
    mv "no-header/slipstream-0.001-1-${VIDEO}_with_http_header.mp4" \
        "with-header/slipstream_0.001_1-${VIDEO}.mpd"
done
