#!/usr/bin/python3
import sys
import os
from collections import namedtuple

Config = namedtuple('Config', 'qualities thresholds orders')
FrameSSIM = namedtuple('FrameSSIM', 'dropped_frames ssim')

THRESHOLDS_SUFFIX = '-thresholds.dat'

class Byterange:
    def __init__(self, start: int, end: int):
        if end < start:
            raise ValueError('Trying to construct invalid range: ' + str(start) + '-' + str(end))
        self.start = start
        self.end = end

    def size(self):
        return self.end - self.start + 1

    def __str__(self):
        return str(self.start) + '-' + str(self.end)


class Segment:
    def __init__(self, raw_line: str):
        self.raw_line = raw_line
        self.unreliable_ranges = list()

    def add_unreliable_range(self, start: int, end: int):
        self.unreliable_ranges.append(Byterange(start, end))

    def sum_ranges(self, to: int):
        ret = 0
        for i in range(to):
            ret += self.unreliable_ranges[i].size()
        return ret

    def frames(self):
        return len(self.unreliable_ranges)

    def __str__(self):
        rep = '['
        for unreliable_range in self.unreliable_ranges:
            rep += str(unreliable_range) + ','
        return rep[:-1] + ']'


class Thresholds:
    def __init__(self):
        # Maps segment_no to a a list of dropped_frames, ssim tupels
        self.__data = dict()
        # Maps segment_no to the chosen order
        self.__orders = dict()

    def add_segment_treshold(self, segment_no: str, dropped_frames: str, ssim: str, order: str):
        if segment_no not in self.__data:
            self.__data[segment_no] = [FrameSSIM(dropped_frames, ssim)]
            if segment_no in self.__orders:
                raise KeyError('Segment number ' + str(segment_no) + ' was already in orders dict, but not in data dict.')
            self.__orders[segment_no] = order
        else:
            self.__data[segment_no].append(FrameSSIM(dropped_frames, ssim))
            if segment_no not in self.__orders:
                raise KeyError('Segment number ' + str(segment_no) + ' was already in data dict, but not in orders dict.')
            if self.__orders[segment_no] != order:
                raise ValueError('Inconsistent order assignment for segment ' + str(segment_n))

    def get_segment_thresholds(self, segment_no: str):
        if segment_no not in self.__data:
            raise KeyError('Trying to access unknown segment no: ' + segment_no)
        return self.__data[segment_no]

    def get_segment_order(self, segment_no: str):
        if segment_no not in self.__orders:
            raise KeyError('Trying to access unknown segment no: ' + segment_no)
        return self.__orders[segment_no]


    def __str__(self):
        return str(self.__data)


class Representation:
    def __init__(self):
        self.base_url = None
        self.segments = list()

    def get_segment(self, segment_no: int):
        return self.segments[segment_no - 1]

    def __str__(self):
        rep = self.base_url + ': segments:' + str(len(self.segments))
        return rep


def read_config(config_path: str):
    qualities = None
    threshold_path = None
    orders = dict()
    with open(config_path, 'r') as f:
        for line in f:
            line_split = line.split(':')
            if len(line_split) != 2:
                raise ValueError('Invalid config line: ' + line.strip())
            if line_split[0] == 'qualities':
                qualities = int(line_split[1].strip())
            elif line_split[0] == 'thresholds':
                threshold_path = line_split[1].strip()
                if not threshold_path.endswith('/'):
                    threshold_path += '/'
                if not os.path.exists(threshold_path):
                    raise FileNotFoundError('Specified path does not exist: ' + threshold_path)
            else:
                order = line_split[0].strip()
                mpd = line_split[1].strip()
                if not os.path.exists(mpd):
                    raise FileNotFoundError('Specified MPD does not exist: ' + mpd)
                orders[order] = mpd
    if qualities is None or threshold_path is None:
        raise ValueError('"qualities" or "thresholds" attribute missing in config.')
    return Config(qualities, threshold_path, orders)


def parse_baseurl(line: str):
    line = line.strip()
    open_tag = '<BaseURL>'
    close_tag = '</BaseURL>'
    open_tag_pos = line.find(open_tag)
    close_tag_pos = line.find(close_tag)
    if open_tag_pos < 0 or close_tag_pos < 0:
        raise ValueError('Invalid BaseURL line: ' + line)
    line = line[open_tag_pos + len(open_tag):close_tag_pos]
    dash_tag = '_dash'
    dash_pos = line.find(dash_tag)
    if dash_pos < 0:
        raise ValueError('Failed to find _dash pattern in BaseURL: ' + line)
    line = line[:dash_pos + len(dash_tag)]
    return line


def parse_segmenturl(line: str):
    ret = Segment(line)
    line_split = line.split()
    unreliable_ranges = None
    for pair in line_split:
        pair_split = pair.split('=')
        if len(pair_split) != 2:
            continue
        if pair_split[0] == 'unreliable':
            unreliable_ranges = pair_split[1].strip('"/>')
            break
    if unreliable_ranges is None:
        raise ValueError('Failed to find unreliable range in SegmentURL: ' + line.strip())
    multi_ranges = unreliable_ranges.split(',')
    for single_range in multi_ranges:
        single_range_split = single_range.split('-')
        if len(single_range_split) != 2:
            raise ValueError('Invalid byterange in SegmentURL: ' + line.strip())
        ret.add_unreliable_range(int(single_range_split[0]), int(single_range_split[1]))
    return ret


def parse_mpd(mpd: str, qualities: int):
    ret = dict()
    curr_representation = None
    # Qualities are zero-indexed
    last_quality = qualities - 1
    with open(mpd, 'r') as f:
        for line in f:
            if 'BaseURL' in line:
                if curr_representation is not None:
                    #ret[curr_representation.base_url] = curr_representation
                    ret[last_quality] = curr_representation
                    last_quality -= 1
                curr_representation = Representation()
                curr_representation.base_url = parse_baseurl(line)
            elif 'SegmentURL' in line:
                curr_representation.segments.append(parse_segmenturl(line))
    #ret[curr_representation.base_url] = curr_representation
    ret[last_quality] = curr_representation
    if last_quality != 0:
        raise ValueError('Missing quality level in MPD. Expected ' + str(qualities) + ' got ' + str(qualities - last_quality))
    return ret


def parse_thresholds(threshold_path: str, quality: int):
    threshold_file = threshold_path + str(quality) + THRESHOLDS_SUFFIX
    ret = Thresholds()
    with open(threshold_file, 'r') as f:
        # Consume header
        f.readline()
        for line in f:
            line_split = line.split()
            if len(line_split) < 5:
                raise ValueError('Invalid threshold line: ' + line.strip())
            ret.add_segment_treshold(line_split[0], line_split[1], line_split[3], line_split[4])
    return ret


def insert_thresholds_attribute(raw_line: str, thresholds: list):
    thresholds_attribute = 'ssims="'
    for i in range(len(thresholds)):
        thresholds_attribute += ':'.join(map(str, thresholds[i])) + ','
    thresholds_attribute = thresholds_attribute[:-1] + '"'
    line_split = raw_line.split()
    line_split.insert(2, thresholds_attribute)
    return '          ' + ' '.join(line_split) + '\n'


def get_threshold_line(segment: Segment, thresholds: list()):
    modified_thresholds = list()
    for threshold in thresholds:
        kept_frames = segment.frames() - int(threshold.dropped_frames)
        required_size = segment.sum_ranges(kept_frames)
        modified_thresholds.append((threshold.ssim, kept_frames, required_size))
    return insert_thresholds_attribute(segment.raw_line, modified_thresholds)



def generate_output(base_mpd: str, order_representations: dict, quality_thresholds: list, output_mpd: str):
    curr_representation = None
    segment_count = 1
    # We start at the highest quality
    quality_count = len(quality_thresholds)
    with open(base_mpd, 'r') as base, open(output_mpd, 'w') as out:
        for line in base:
            if 'BaseURL' in line:
                quality_count -= 1
                segment_count = 1
            elif 'SegmentURL' in line:
                segment_order = quality_thresholds[quality_count].get_segment_order(str(segment_count))
                segment_thresholds = quality_thresholds[quality_count].get_segment_thresholds(str(segment_count))
                segment = order_representations[segment_order][quality_count].get_segment(segment_count)
                # Thresholds should be lowest SSIM to highest SSIM in the MPD
                segment_thresholds.reverse()
                out.write(get_threshold_line(segment, segment_thresholds))
                segment_count += 1
                continue
            out.write(line)
    if quality_count != 0:
        raise ValueError('Missing quality level in MPD. Expected ' + str(len(quality_thresholds)) + ' got ' + str(len(quality_thresholds) - quality_count))


if __name__ == '__main__':
    if len(sys.argv) != 3:
        print('usage: ' + sys.argv[0] + ' <config> <output-mpd>')
        exit(1)
    config_path = sys.argv[1]
    output_path = sys.argv[2]
    config = read_config(config_path)
    order_representations = dict()
    for order in config.orders:
        order_representations[order] = parse_mpd(config.orders[order], config.qualities)
    quality_thresholds = list()
    for q in range(config.qualities):
        quality_thresholds.append(parse_thresholds(config.thresholds, q))
    generate_output(config.orders['original'], order_representations, quality_thresholds, output_path)
