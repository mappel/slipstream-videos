#!/usr/bin/python3
# This script merges multiple MPD files into one with multiple representations.
#
# usage: ./merge_mpd.py <output> <MPD 1> [<MPD 2> ...]
#
# IMPORTANT: The order of arguments does matter! To get a correct output
# the MPD files need to be passed in order of decreasing quality.
#
# The script also corrects the <BaseURL> tag from the *dashinit.mp4 format to
# the *dashinit_with_http_header.mp4 format.

import sys

def add_representation(to_file, from_file, from_file_name: str, r_id: int):
    video_name = from_file_name[:-4] + 'init_with_http_header.mp4'
    base_url_line = '    <BaseURL>' + video_name + '</BaseURL>\n'
    line = from_file.readline()
    # Scroll until opening <Representation> tag is found.
    while 'Representation' not in line:
        line = from_file.readline()
    # Default id is '1' so replace first occurence with new id
    rep_line = line.replace('1', str(r_id), 1)
    to_file.write(rep_line)
    line = from_file.readline()
    if 'BaseURL' not in line:
        print('Error: Expected <BaseURL> after <Representation> but got this:', file=sys.stderr)
        print(line.strip(), file=sys.stderr)
        exit(1)
    to_file.write(base_url_line)
    line = from_file.readline()
    # Copy all lines until closing </Representation> tag is found
    while 'Representation' not in line:
        to_file.write(line)
        line = from_file.readline()
    # Write closing tag and additional newline for spacing
    to_file.write(line + '\n')


if len(sys.argv) < 3:
    print('usage: ' + sys.argv[0] + ' <output> <MPD 1> [<MPD 2> ...]')
    exit(1)
representation_id = 1
out = sys.argv[1]
base = sys.argv[2]
outfile = open(out, 'w')
basefile = open(base, 'r')
line = basefile.readline()
# Copy header of basefile
while 'AdaptationSet' not in line:
    outfile.write(line)
    line = basefile.readline()
outfile.write(line)
# Add representation set of basefile (buffer already points to correct line)
add_representation(outfile, basefile, base, representation_id)
representation_id += 1
# Add representation sets of remaining files
for i in range(3, len(sys.argv)):
    mpd_file_name = sys.argv[i]
    with open(mpd_file_name, 'r') as mpd_file:
        add_representation(outfile, mpd_file, mpd_file_name, representation_id)
    representation_id += 1
# Copy footer from basefile
outfile.writelines(basefile.readlines())
basefile.close()
outfile.close()
