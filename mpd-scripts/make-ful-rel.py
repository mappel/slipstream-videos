#!/usr/bin/python3
import sys
import re

if len(sys.argv) != 3:
    print('usage: ' + sys.argv[0] + ' <input MPD> <output MPD>')
    exit(1)
infile_name = sys.argv[1]
outfile_name = sys.argv[2]
media_range_end_match = re.compile('\\d+')
infile = open(infile_name, 'r')
outfile = open(outfile_name, 'w')
for line in infile:
    if 'mediaRange' not in line:
        outfile.write(line)
        continue
    line_split = line.split()
    media_range_idx = -1
    media_range_start = -1
    media_range_end = -1
    for i in range(0, len(line_split)):
        if line_split[i].startswith('mediaRange'):
            match_result = media_range_end_match.findall(line_split[i])
            if not match_result or len(match_result) != 2:
                print('Error: Invalid media range: ' + line_split[i], file=sys.stderr)
                exit(1)
            media_range_start = int(match_result[0])
            media_range_end = int(match_result[1])
            media_range_idx = i
            line_split[i] = line_split[i].rstrip('/>')
    if media_range_end == -1:
        print('Error: Failed to find mediaRange attribute in line:', file=sys.stderr)
        print(line.strip(), file=sys.stderr)
        exit(1)
    reliable_range_str = 'reliable="' + str(media_range_start) + '-' + str(media_range_end) + '"'
    reliable_size_str = 'reliableSize="' + str(media_range_end - media_range_start + 1) + '"'
    line_split.insert(media_range_idx + 1, reliable_range_str)
    line_split.insert(media_range_idx + 2, reliable_size_str)
    new_line = '      ' + ' '.join(line_split)
    if not new_line.endswith('/>'):
        new_line += '/>'
    new_line += '\n' 
    outfile.write(new_line)
infile.close()
outfile.close()
