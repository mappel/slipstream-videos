#!/usr/bin/python3
import os
import sqlite3
import sys
from collections import namedtuple

VideoStats = namedtuple('VideoStats', 'orders qualities segments frames')
FrameSSIM = namedtuple('FrameSSIM', 'frames percentage ssim order')

def get_video_stats(db_cursor: sqlite3.Cursor, video: str):
    orders_stmt = 'SELECT DISTINCT(frame_order) FROM data WHERE video = ?'
    max_stmt = 'SELECT MAX(quality), MAX(segment_no), MAX(frames_dropped) FROM data WHERE video = ?'
    orders = list()
    for row in db_cursor.execute(orders_stmt, (video,)):
        orders.append(row[0])
    res = db_cursor.execute(max_stmt, (video,)).fetchone()
    return VideoStats(orders, res[0], res[1], res[2])


def calculate_thresholds_for_quality(db_cursor: sqlite3.Cursor, video: str, nth_frame: int, min_ssim_distance: float, segment_no: int, quality: int):
    min_ssim = 0
    if quality > 0:
        # We can do LIMIT 1 since it does not matter which order we choose.
        ssim_stmt = 'SELECT ssim FROM data WHERE video = ? AND quality = ? AND segment_no = ? and frames_dropped = 0 LIMIT 1'
        min_ssim = db_cursor.execute(ssim_stmt, (video, quality - 1, segment_no)).fetchone()[0]
    # The fetched row contains the order with the highest drop percentage. In
    # case of a tie, the alphabetically first order is chosen.
    best_order_stmt = 'SELECT frame_order, frames_dropped FROM data WHERE video = ? AND quality = ? AND segment_no = ? AND ssim >= ? AND frames_dropped % ? = 0 ORDER BY percentage_dropped DESC, frame_order ASC LIMIT 1'
    res = db_cursor.execute(best_order_stmt, (video, quality, segment_no, min_ssim, nth_frame)).fetchone()
    relevant_ssims = list()
    if res is None:
        # Depending on the encoding levels, there can be anomalies where the lower quality actually has a higher SSIM.
        # In this case the query above does not return anything since min_ssim is higher than the highest SSIM for the
        # current quality.
        backup_stmt = 'SELECT frame_order, ssim FROM data WHERE video = ? AND quality = ? AND segment_no = ? AND frames_dropped = 0 ORDER BY frame_order ASC LIMIT 1'
        backup_res = db_cursor.execute(backup_stmt, (video, quality, segment_no)).fetchone()
        print('Warning: Quality', quality - 1, 'has a higher SSIM than quality', quality, min_ssim, '>', backup_res[1], file=sys.stderr)
        relevant_ssims.append(FrameSSIM(0, 0.0, backup_res[1], backup_res[0]))
    else:
        best_order = res[0]
        best_frames_dropped = res[1]
        relevant_ssims_stmt = 'SELECT frames_dropped, percentage_dropped, ssim FROM data WHERE video = ? AND quality = ? AND segment_no = ? AND frame_order = ? AND frames_dropped <= ? AND frames_dropped % ? = 0 ORDER BY frames_dropped'
        for row in db_cursor.execute(relevant_ssims_stmt, (video, quality, segment_no, best_order, best_frames_dropped, nth_frame)):
            relevant_ssims.append(FrameSSIM(row[0], row[1], row[2], best_order))
    filtered_ssims = [relevant_ssims[-1]]
    last_ssim = filtered_ssims[0].ssim
    for val in reversed(relevant_ssims[:-1]):
        if val.ssim >= last_ssim + min_ssim_distance:
            filtered_ssims.append(val)
            last_ssim = val.ssim
    # Always include zero dropped frames
    if filtered_ssims[-1].frames != 0:
        filtered_ssims.append(relevant_ssims[0])
    filtered_ssims.reverse()
    return filtered_ssims


def calculate_segment_thresholds(db_cursor: sqlite3.Cursor, video: str, stats: VideoStats, nth_frame: int, min_ssim_distance: float, segment_no: int):
    ret = dict()
    for quality in range(stats.qualities, -1, -1):
        ret[quality] = calculate_thresholds_for_quality(db_cursor, video, nth_frame, min_ssim_distance, segment_no, quality)
    return ret


def write_output(output_path: str, quality: int, relevant_ssims: dict):
    with open(output_path + str(quality) + '-thresholds.dat', 'w') as t, open(output_path + str(quality) + '-steps.dat', 'w') as s:
        t.write('segment_no dropped_frames dropped_percentage ssim order\n')
        s.write('segment_no steps\n')
        for segment_no in sorted(relevant_ssims.keys()):
            ssims = relevant_ssims[segment_no]
            s.write(str(segment_no) + ' ' + str(len(ssims)) + '\n')
            for val in ssims:
                line = [segment_no, val.frames, val.percentage, val.ssim, val.order]
                t.write(' '.join(map(str, line)) + '\n')


if __name__ == "__main__":
    if len(sys.argv) != 6:
        print('usage:', sys.argv[0], '<ssim-db> <video> <use-nth-frame> <min-ssim-distance> <path/to/output>', file=sys.stderr)
        exit(1)
    database = sys.argv[1]
    video = sys.argv[2]
    nth_frame = int(sys.argv[3])
    min_ssim_distance = float(sys.argv[4])
    output_path = sys.argv[5]
    if not output_path.endswith('/'):
        output_path += '/'
    db_connection = sqlite3.connect(database)
    db_cursor = db_connection.cursor()
    stats = get_video_stats(db_cursor, video)
    print('video:', video)
    print('orders:', stats.orders)
    print('max q:', stats.qualities)
    print('segments:', stats.segments)
    print('max dropped frames:', stats.frames)
    quality_map = dict()
    for segment_no in range(1, stats.segments + 1):
        print(segment_no)
        segment_ssims = calculate_segment_thresholds(db_cursor, video, stats, nth_frame, min_ssim_distance, segment_no)
        for quality in segment_ssims:
            if quality not in quality_map:
                quality_map[quality] = dict()
            quality_map[quality][segment_no] = segment_ssims[quality]
    for quality in quality_map:
        write_output(output_path, quality, quality_map[quality])

