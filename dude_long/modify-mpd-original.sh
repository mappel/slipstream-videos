#!/bin/bash
MPD=slipstream-original-dude_long.mpd
mv "$MPD" interm
cd interm
for vid in *dashinit.mp4
do
	PREFIX=${vid%_dashinit.mp4}
	/pool/data/mappel/header_parser/build/header_parser "$vid" --mpd "$MPD"
done
mv "$MPD" ..
