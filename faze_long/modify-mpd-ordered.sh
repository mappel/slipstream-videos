#!/bin/bash
MPD=slipstream-ordered-faze_long.mpd
mv "$MPD" interm
cd interm
for vid in *dashinit.mp4
do
	PREFIX=${vid%_dashinit.mp4}
	/pool/data/mappel/header_parser/build/header_parser "$vid" --mpd "$MPD" --weights "$PREFIX"-ordered-weights/"$PREFIX"
done
mv "$MPD" ..
